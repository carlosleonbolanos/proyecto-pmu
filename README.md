# <img src="https://assets.gitlab-static.net/uploads/-/system/project/avatar/11698184/avatar.png" width="75px"></img> Proyecto PMU <img src="https://assets.gitlab-static.net/uploads/-/system/project/avatar/11698184/avatar.png" width="75px"></img>
### Carlos León Bolaños

---

## __**Presentación**__
En este repositorio se encuentran el código fuente, informes y vídeo sobre el proyecto para el Módulo PMU de 2º de CFGS de Sistemas de Telecomunicaciones e Informáticos. A continuación se presentan el vídeo demostrativo del proyecto, así como de los documentos presentes en este repositorio.

![Vídeo Presentación](video/presentation.mp4)

<p style="text-align: center;">En caso de no poder visualizar correctamente el vídeo, haga click en el siguiente enlace:</p>
<p style="text-align: center;">[VER EN YOUTUBE](https://www.youtube.com/watch?v=6SHbi7lFs0w)</p>

- [Informe del Proyecto (PDF) - https://gitlab.com/carlosleonbolanos/proyecto-pmu/blob/master/documents/Informe.pdf](https://gitlab.com/carlosleonbolanos/proyecto-pmu/blob/master/documents/Informe.pdf).
- [Diagramas de flujo del proyecto (PDF) - https://gitlab.com/carlosleonbolanos/proyecto-pmu/blob/master/documents/Diagramas_de_Flujo.pdf](https://gitlab.com/carlosleonbolanos/proyecto-pmu/blob/master/documents/Diagramas_de_Flujo.pdf)
- [Esquema de conexión (PNG) - https://gitlab.com/carlosleonbolanos/proyecto-pmu/blob/master/documents/schematic.png](https://gitlab.com/carlosleonbolanos/proyecto-pmu/blob/master/documents/schematic.png)
- [Pins del Arduino UNO R3 (PNG) - https://gitlab.com/carlosleonbolanos/proyecto-pmu/blob/master/documents/arduino_pinout.png](https://gitlab.com/carlosleonbolanos/proyecto-pmu/blob/master/documents/arduino_pinout.png)
- Esquema de Módulo GSM de TinySine
  * [Página 1 - https://gitlab.com/carlosleonbolanos/proyecto-pmu/blob/master/documents/TinySine_GSMSHIELD-page-1.jpg](https://gitlab.com/carlosleonbolanos/proyecto-pmu/blob/master/documents/TinySine_GSMSHIELD-page-1.jpg)
  * [Página 2 - https://gitlab.com/carlosleonbolanos/proyecto-pmu/blob/master/documents/TinySine_GSMSHIELD-page-2.jpg](https://gitlab.com/carlosleonbolanos/proyecto-pmu/blob/master/documents/TinySine_GSMSHIELD-page-2.jpg)