/* 
  PROYECTO PMU - TELECONTROL Y TELEMETRÍA POR GSM
  ===============================================

  - Autor:
    Carlos León Bolaños (CIAL: A84A07009B)

  - Materia:
    Proyecto de sistemas de telecomunicaciones e informáticos (PMU)

  - Curso:
    2º CFGS Sistemas de Telecomunicaciones e Informática

  - Centro:
    C.I.F.P. César Manrique

  - Descripción:
    Proyecto de Arduino para el recibimiento y redirección de SMS y control del sistema a través SMS. Este código fuente está preparado para un sistema Arduino con los siguientes elementos:
    * Tinysine GSM/GRPS Shield con controlador SimCom SIM900.
    * Display 20x4 2004A.
    * Interfaz I2C para Display.
    * 4 x Led 10mm RGB.
    * 4 x Resistencia 120ohms
    Este código fue desarrollado con Microsoft(tm) Visual Studio Code (https://code.visualstudio.com/) y la extensión PlatformIO(tm) (https://platformio.org/) y alojado en GITLAB (https://gitlab.com/carlosleonbolanos/proyecto-pmu). Para más detalles, consultar el archivo README.md o README.pdf para más detalles.

  - Licencia:
    Licencia GNU AGPLv3 - Copyright (c) 2019 Carlos León Bolaños (más detalles contenidos en el archivo LICENSE o en https://www.gnu.org/licenses/agpl-3.0.en.html)

  - Licencias de Librerías externas utilizadas:
    * LiquidCrystal_I2C by Frank de Brabander - Más detalles en https://github.com/fdebrabander/Arduino-LiquidCrystal-I2C-library
*/

// -= DECLARACIONES DE REFERENCIAS A LIBRERÍAS=-
#include <Arduino.h>           // Microprocesadores ATMEL con bootloader Arduino
#include <GSM.h>               // Módulo GSM
#include <Wire.h>              // Protocolo de comunicación I2C
#include <LiquidCrystal_I2C.h> // Uso de displays de 16x2, 16x4 o 20x4 por medio del protocolo de comunicación I2C

// -= DECLARACIONES DE DEFINICIONES =-
// - Definiciones GSM
#define GSM_PIN "1984"            // Clave PIN de la SIM
#define GSM_SECURE "+34638193244" // Número de teléfono autorizado

// - Definiciones LED
#define LED_SMS_RCVD 15  // SMS Recibido
#define LED_SMS_SEC 16   // SMS de número autorizado
#define LED_SMS_NOSEC 17 // SMS de número no autorizado
#define LED_CMD_RCVD 5   // Comando recibido
#define LED_CMD_OPEN 6   // Comando Apertura
#define LED_CMD_CLOSE 4  // Comando Cierre

// -= DECLARACIONES DE VARIABLES =-
// - Variable Serial

// - Variables LCD
LiquidCrystal_I2C LCD(0x3f, 20, 4); // Inicializando variable para el control del display

// - Variables GSM
GSM GSM_GSM;     // Comunicación con el módulo GSM
GSM_SMS GSM_MSG; // Controles para envío y recepción de SMS

// -= FUNCIONES PERSONALIZADAS =-
/**
   * Función de envío de SMS a número de seguridad; 
   *
   * @param MSG: Contenido del SMS en formato String (cadena de texto); 
   * @return: Devuelve un valor booleano (true o false).
   */
bool SEND_SMS(String MSG)
{
  GSM_MSG.beginSMS(GSM_SECURE);
  GSM_MSG.print(MSG);
  int SEND_TRY = GSM_MSG.endSMS();
  if ((SEND_TRY == 1) || SEND_TRY == 4)
  {
    LCD.setCursor(0, 3);
    LCD.print("    Envio SMS OK    ");
    delay(2000);
    return true;
  }
  else if ((SEND_TRY == 2) || (SEND_TRY == 3))
  {
    LCD.setCursor(0, 3);
    LCD.print("  Error envio SMS.  ");
    delay(2000);
    return false;
  }
  return false;
}

/**
   * Función de inicio del módulo GSM.
   *
   * @return Devuelve un valor booleano (true o false).
   */
bool START_GSM()
{
  if (GSM_GSM.secureShutdown())
  {
    digitalWrite(8, HIGH); // Se establece valor HIGH a ls salida para enceder el módulo GSM
    delay(1100);           // Se mantiene el valor durante 1,1 segundos
    digitalWrite(8, LOW);  // se revierte el valor de la salida a LOW
  }
  bool GSM_CONN = false;
#ifdef GSM_PIN
  while (!GSM_CONN) // Comienza un bucle que no se romperá hasta que el valor de la variable GSM_CONN sea TRUE
  {
    if (GSM_GSM.begin(GSM_PIN) == GSM_READY) // Se verifica estado de la SIM y la red en caso de haber pin configurado en ella
    {
      GSM_CONN = true; // Al verificarse que la función GSM_GSM.begin() nos devuelve el valor GSM_READY (o 3), se cambia el valor del GSM_CONN a TRUE para romper el bucle
    }
  }
#else
  while (!GSM_CONN) // Comienza un bucle que no se romperá hasta que el valor de la variable GSM_CONN sea TRUE
  {
    if (GSM_GSM.begin() == GSM_READY) // Se verifica estado de la SIM y la red
    {
      GSM_CONN = true; // Al verificarse que la función GSM_GSM.begin() nos devuelve el valor GSM_READY (o 3), se cambia el valor del GSM_CONN a TRUE para romper el bucle
    }
  }
#endif
  LCD.setCursor(0, 2);
  LCD.print("Comprobando srv. SMS");
  LCD.setCursor(0, 2);
  LCD.print("                    ");
  if (SEND_SMS("Proyecto Arduino de CLB listo"))
  {
    LCD.setCursor(0, 2);
    LCD.print("                    ");
    return true;
  }
  else
  {
    LCD.setCursor(0, 2);
    LCD.print("                    ");
    return false;
  }
  return false;
}

// -= CONFIGURACIÓN INICIAL =-
void setup()
{
  LCD.init();      // Comienzo comunicación con interfaz Display
  LCD.backlight(); // Encendido de iluminación
  LCD.home();      // Estableciendo cursor en origen
  LCD.print(" Proyecto PMU - CLB ");
  LCD.setCursor(0, 1);
  LCD.print("  CIAL: A84A07009B  ");

  pinMode(LED_CMD_RCVD, OUTPUT);
  pinMode(LED_CMD_OPEN, OUTPUT);
  pinMode(LED_CMD_CLOSE, OUTPUT);
  pinMode(LED_SMS_RCVD, OUTPUT);
  pinMode(LED_SMS_SEC, OUTPUT);
  pinMode(LED_SMS_NOSEC, OUTPUT);
  pinMode(8, OUTPUT);

  START_GSM(); // Llama la función "START_GSM" que inicia y devuelve el estado del módem
}

// -= BUCLE =-
void loop()
{
  LCD.setCursor(0, 3);
  LCD.print("       Listo.       ");
  if (GSM_MSG.available())
  {
    digitalWrite(LED_SMS_RCVD, HIGH);
    delay(500);
    char GSM_SENDER[20];
    char SMS;
    GSM_MSG.remoteNumber(GSM_SENDER, 20);
    String SENDER(GSM_SENDER);
    if (SENDER == GSM_SECURE)
    {
      digitalWrite(LED_SMS_RCVD, LOW);
      digitalWrite(LED_SMS_SEC, HIGH);
      SMS = GSM_MSG.read();
      int CMD = SMS - '0';
      if (CMD == 0)
      {
        LCD.setCursor(0, 2);
        LCD.print("  Comando recibido  ");
        LCD.setCursor(0, 3);
        LCD.print("  Cerrando valvula  ");
        digitalWrite(LED_CMD_RCVD, HIGH);
        for (int i = 0; i < 6; i++)
        {
          digitalWrite(LED_CMD_CLOSE, HIGH);
          delay(1000);
          digitalWrite(LED_CMD_CLOSE, LOW);
          delay(500);
        }
        SEND_SMS("Valvula cerrada");
        LCD.setCursor(0, 2);
        LCD.print("                    ");
        LCD.setCursor(0, 3);
        LCD.print("                    ");
        digitalWrite(LED_CMD_RCVD, LOW);
      }
      else if (CMD == 1)
      {
        LCD.setCursor(0, 2);
        LCD.print("  Comando recibido  ");
        LCD.setCursor(0, 3);
        LCD.print("  Abriendo valvula  ");
        digitalWrite(LED_CMD_RCVD, HIGH);
        for (int i = 0; i < 6; i++)
        {
          digitalWrite(LED_CMD_OPEN, HIGH);
          delay(1000);
          digitalWrite(LED_CMD_OPEN, LOW);
          delay(500);
        }
        SEND_SMS("Valvula abierta");
        LCD.setCursor(0, 2);
        LCD.print("                    ");
        LCD.setCursor(0, 3);
        LCD.print("                    ");
        digitalWrite(LED_CMD_RCVD, LOW);
      }
      else
      {
        LCD.setCursor(0, 2);
        LCD.print("  Comando recibido  ");
        LCD.setCursor(0, 3);
        LCD.print("  Comando erroneo!  ");
        digitalWrite(LED_CMD_RCVD, HIGH);
        for (int i = 0; i < 6; i++)
        {
          digitalWrite(LED_CMD_OPEN, HIGH);
          delay(1000);
          digitalWrite(LED_CMD_OPEN, LOW);
          digitalWrite(LED_CMD_CLOSE, HIGH);
          delay(1000);
          digitalWrite(LED_CMD_CLOSE, LOW);
          delay(1000);
        }
        digitalWrite(LED_CMD_RCVD, LOW);
        SEND_SMS("Comando erroneo, seleccione: 0 - Cerrar valvula; 1 - Abrir valvula.");
        LCD.setCursor(0, 2);
        LCD.print("                    ");
        LCD.setCursor(0, 3);
        LCD.print("                    ");
      }
      digitalWrite(LED_SMS_SEC, LOW);
    }
    else
    {
      LCD.setCursor(0, 2);
      LCD.print("    SMS recibido    ");
      LCD.setCursor(0, 3);
      LCD.print("   Reenviando SMS   ");
      digitalWrite(LED_SMS_NOSEC, HIGH);
      delay(500);
      String SMS_STR = "";
      while (SMS = GSM_MSG.read())
      {
        SMS_STR += SMS;
      }
      if (SEND_SMS(SENDER + " - " + SMS_STR))
      {
        digitalWrite(LED_SMS_NOSEC, LOW);
        LCD.setCursor(0, 2);
        LCD.print("                    ");
        LCD.setCursor(0, 3);
        LCD.print("                    ");
      }
      else
      {
        for (int i = 0; i < 4; i++)
        {
          digitalWrite(LED_SMS_NOSEC, HIGH);
          delay(500);
          digitalWrite(LED_SMS_NOSEC, LOW);
          delay(500);
        }
        LCD.setCursor(0, 2);
        LCD.print("     ¡¡ERROR!!:     ");
        LCD.setCursor(0, 3);
        LCD.print("  SMS no reenviado  ");
        delay(2000);
        LCD.setCursor(0, 2);
        LCD.print("                    ");
        LCD.setCursor(0, 3);
        LCD.print("                    ");
      }
      digitalWrite(LED_SMS_RCVD, LOW);
    }
    GSM_MSG.flush();
  }
  delay(500);
}
